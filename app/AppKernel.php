<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new scrclub\CMSBundle\scrclubCMSBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),    // Check its existing or add
            new A2lix\TranslationFormBundle\A2lixTranslationFormBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            //new scrclub\AcraBundle\scrclubAcraBundle(),
            new scrclub\TvkBundle\scrclubTvkBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),
            //new HWI\Bundle\OAuthBundle\HWIOAuthBundle()
          //  new FOS\TwitterBundle\FOSTwitterBundle()
            //new scrclub\VRBundle\scrclubVRBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
